package com.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BootstrapDataInitializer implements InitializingBean {

	private final Logger logger = LoggerFactory.getLogger(BootstrapDataInitializer.class);

	@Autowired
	private MovieRepository repository;

	@Override
	@Transactional()
	public void afterPropertiesSet() throws Exception {
		logger.info("Bootstrapping data...");

		createMovies();

		logger.info("...Bootstrapping completed");
	}

	private void createMovies() {
		logger.info("Creating movies...");

		createMovie("Pulp Fiction", "Quentin Tarantino", 1994);
		createMovie("Inception", "Christopher Nolan", 2010);
		createMovie("Mr. Nobody", "Jaco Van Dormael", 2009);
		createMovie("Requiem for a Dream", "Darren Aronofsky", 2000);
		createMovie("Fight Club", "David Fincher", 1999);
		createMovie("Underground", "Emir Kusturica", 1995);
		createMovie("The Matrix", "The Wachowski Brothers", 1999);

		logger.info("...Creation of movies completed");
	}

	private void createMovie(String title, String director, int year) {
		Movie movie = new Movie();
		movie.setId(null);
		movie.setTitle(title);
		movie.setDirector(director);
		movie.setYear(year);
		repository.saveAndFlush(movie);
	}
}