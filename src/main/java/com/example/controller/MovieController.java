package com.example;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
@RequestMapping("/api/movies")
public class MovieController {

	@Autowired
	private MovieRepository repository;

	@RequestMapping(method = RequestMethod.GET)
	public List findMovies() {
		return repository.findAll();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/year/{year}")
	public List findMoviesByYear(@PathVariable int year) {
		return repository.findByYear(year);
	}

	@RequestMapping(method = RequestMethod.GET, value = "{id}")
	public Movie findMovie(@PathVariable Long id) {
		return repository.findOne(id);
	}

	@RequestMapping(method = RequestMethod.POST)
	public Movie addMovie(@RequestBody Movie movie) {
		movie.setId(null);
		return repository.saveAndFlush(movie);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "{id}")
	public Movie updateMovie(@RequestBody Movie movie, @PathVariable Long id) {
		movie.setId(id);
		return repository.saveAndFlush(movie);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "{id}")
	public void deleteMovie(@PathVariable Long id) {
		repository.delete(id);
	}
}