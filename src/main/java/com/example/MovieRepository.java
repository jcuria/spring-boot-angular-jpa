package com.example;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

interface MovieRepository extends JpaRepository<Movie, Long> {
	
	List<Movie> findByYear(int year);
}