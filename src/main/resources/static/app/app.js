var movieApp = angular.module('myApp', []);

movieApp.controller('AppController', ['$scope', '$http', function($scope,$http) {

	$http.get('api/movies').success(function (data) {
        $scope.movies = data;
    });

    $scope.addMovie = function(newMovie) {
    	var data = {'title': newMovie.title, 'director': newMovie.director, 'year': newMovie.year};
    	$http.post('api/movies', data).success(function (data) {
    		$scope.movies.push(data);
    		$scope.message = 'Movie created successfuly';
    		$scope.newMovie.title = '';
    		$scope.newMovie.director = '';
            $scope.newMovie.year = '';
    	});
    };

    $scope.deleteMovie = function(movie) {
    	if (confirm('Are you sure?')) {
    		$http.delete('api/movies/' + movie.id).success(function (data) {
    			var index = $scope.movies.indexOf(movie);
  				$scope.movies.splice(index, 1);
    			$scope.message = 'Movie deleted successfuly';
    		});
    	}
    };
}]);
	