# Spring Boot Api #

This is an example of a Web API using Spring Boot, AngularJS and JPA.

### What is used ###

* Spring Boot
* Maven 3
* JDK 1.7
* AngularJS 1.5.3
* Bower 1.7.7

### How to run this app ###

* Build: ./mvn clean package
* Run: java -jar target/movie-manager-0.0.1-SNAPSHOT.jar

### Access ##
http://localhost:8080/

### Api ###
http://localhost:8080/api/movies